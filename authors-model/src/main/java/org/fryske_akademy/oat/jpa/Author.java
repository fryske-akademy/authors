/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fryske_akademy.oat.jpa;

import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Access(AccessType.FIELD)
public class Author extends AbstractEntity {
/*
id|berne|ferstoarn|namme|foarnamme|slachte|memmetaal|opjeftetaal|berteplak|skoalleplak|wenplak|opjefteplak|wenne_fan|wenne_oant|berteplak_mem|berteplak_heit|berop|berop_heit|koade

 */

    private String birth, death, surname, forename, sex, motherTongue, profession, professionDad, code;

    private Place placeBirth, placeSchool, residence, saidResidence, placeBirthDad, placeBirthMom;

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getDeath() {
        return death;
    }

    public void setDeath(String death) {
        this.death = death;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMotherTongue() {
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfessionDad() {
        return professionDad;
    }

    public void setProfessionDad(String professionDad) {
        this.professionDad = professionDad;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Place getPlaceBirth() {
        return placeBirth;
    }

    public void setPlaceBirth(Place placeBirth) {
        this.placeBirth = placeBirth;
    }

    public Place getPlaceSchool() {
        return placeSchool;
    }

    public void setPlaceSchool(Place placeSchool) {
        this.placeSchool = placeSchool;
    }

    public Place getResidence() {
        return residence;
    }

    public void setResidence(Place residence) {
        this.residence = residence;
    }

    public Place getSaidResidence() {
        return saidResidence;
    }

    public void setSaidResidence(Place saidResidence) {
        this.saidResidence = saidResidence;
    }

    public Place getPlaceBirthDad() {
        return placeBirthDad;
    }

    public void setPlaceBirthDad(Place placeBirthDad) {
        this.placeBirthDad = placeBirthDad;
    }

    public Place getPlaceBirthMom() {
        return placeBirthMom;
    }

    public void setPlaceBirthMom(Place placeBirthMom) {
        this.placeBirthMom = placeBirthMom;
    }
}
