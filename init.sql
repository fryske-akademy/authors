use authors;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `born` char(10) DEFAULT NULL,
  `died` char(10) DEFAULT NULL,
  `surname` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `firstname` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `native_language` char(1) DEFAULT NULL,
  `indicated_language` char(1) DEFAULT '',
  `birthplace` char(255) CHARACTER SET utf8 COLLATE utf8_bin,
  `schoolplace` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `residence` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `indicated_place` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lived_from` char(4) DEFAULT NULL,
  `lived_to` char(4) DEFAULT NULL,
  `birthplace_mother` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `birthplace_father` char(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `profession` char(255) DEFAULT NULL,
  `profession_father` char(255) DEFAULT NULL,
  `code` char(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) DEFAULT CHARSET=utf8;


insert into author select * from tdb2.auteur; 
